package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
)

const port = "80"

func main() {
	log.Printf("Starting server on port %s\n", port)

	http.HandleFunc("/", HelloServer)
	http.ListenAndServe(fmt.Sprintf(":%s", port), nil)
}

func HelloServer(w http.ResponseWriter, r *http.Request) {
	currentTime := time.Now()
	name := r.URL.Path[1:]
	log.Printf("Handling request for name %s at time %s\n", name, currentTime)

	fmt.Fprintf(w, "Hello go-service, %s!\n", name)
	fmt.Fprint(w, currentTime)
}
